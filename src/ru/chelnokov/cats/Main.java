package ru.chelnokov.cats;

public class Main {
    public static void main(String args[]) {
        Cat misha = new Cat("Миша", "кот");
        System.out.println(misha);
        Cat laska = new Cat("Ласка", "кошка");
        System.out.println(laska);
        Cat it = new Cat("Оно", "что-то непонятное науке");
        System.out.println(it);
    }
}
