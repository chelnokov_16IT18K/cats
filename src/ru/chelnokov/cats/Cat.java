package ru.chelnokov.cats;


public class Cat {
    /**
     * Кличка
     */
    private String name;

    /**
     * Пол
     */
    private String gender;


    Cat(String name, String gender) {
        this.name = name;
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "Привет, я " + gender + ".Меня обычно зовут " + name;
    }

}
